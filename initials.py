# imports for functions needed
from __future__ import print_function
from six.moves import input
import sys
import time
import copy
import rospy
import moveit_commander
import moveit_msgs.msg
import geometry_msgs.msg

# Set up the nodes, topics, and proper settings to control UR5e
moveit_commander.roscpp_initialize(sys.argv)
rospy.init_node("move_group_python_interface_tutorial", anonymous=True)
robot = moveit_commander.RobotCommander()
scene = moveit_commander.PlanningSceneInterface()
group_name = "manipulator"
move_group = moveit_commander.MoveGroupCommander(group_name)
display_trajectory_publisher = rospy.Publisher(
    "/move_group/display_planned_path",
    moveit_msgs.msg.DisplayTrajectory,
    queue_size=20,
)

# Functions
def moving(wpose, x, y, z):
    # Setup variables
    scale = 1.0
    # set the wpose
    wpose.position.x += scale * x
    wpose.position.y += scale * y
    wpose.position.z += scale * z
    # return the modified wpose
    return copy.deepcopy(wpose)

def draw_now(waypoints):
    plan, fraction = move_group.compute_cartesian_path(
        waypoints, 0.01, 0.0  # waypoints to follow  # eef_step
    )  # jump_threshold
    move_group.execute(plan, wait=True)
    return

# Set the initial pose of the robot to draw on the yz-plane and 
# at the bottom left of letter "A" and at +0.2 away from the x-axis
pose_goal = geometry_msgs.msg.Pose()
pose_goal.orientation.w = 1.0
pose_goal.position.x = 0.2
pose_goal.position.y = 0.1
pose_goal.position.z = 0.1
move_group.set_pose_target(pose_goal)
success = move_group.go(wait=True)
move_group.stop()
move_group.clear_pose_targets()

# Prompt for the user/viewer to align the axes
input("Align x-axis out of screen, y-axis right of screen, z-axis top of screen. Press enter to continue.")

waypoints = []
wpose = move_group.get_current_pose().pose

# Below is the first letter "A"
# [Move 1: Left diagonal "/"]
waypoints.append(moving(wpose, 0, 0.1, 0.2))
# [Move 2: Right diagonal "\"]
waypoints.append(moving(wpose, 0, 0.1, -0.2))
# [Move 3: Moving back up to the middle]
waypoints.append(moving(wpose, 0, -0.05, 0.1))
# [Move 4: Draw middle bar "-"]
waypoints.append(moving(wpose, 0, -0.1, 0))
# Draw
draw_now(waypoints)

# Reset to starting position for "L", prepare for next letter, clear waypoints
waypoints.clear()
print("Setting up next letter L. Pause for 1 second.")
time.sleep(1)
# [Reset 1: Draw slant bar "/"]
waypoints.append(moving(wpose, 0, 0.05, 0.1))
# [Reset 2: Move to the left "-"]
waypoints.append(moving(wpose, 0, -0.1, 0))
draw_now(waypoints)

# Below is the second letter "L"
# [Move 1: Move from top to bottom "|"]
waypoints.clear()
print("Starting drawing letter L. After 1 second")
time.sleep(1)
waypoints.append(moving(wpose, 0, 0, -0.2))
# [Move 2: Move from left to right "_"]
waypoints.append(moving(wpose, 0, 0.1, 0))
draw_now(waypoints)

# Reset to starting position for "H", prepare for next letter, clear waypoints
waypoints.clear()
print("Setting up next letter H. Pause for 1 second.")
time.sleep(1)
# [Reset 1: Draw slant bar "\"]
waypoints.append(moving(wpose, 0, -0.1, 0.2))
draw_now(waypoints)


# Below is the third letter "H"
waypoints.clear()
print("Starting drawing letter H. After 1 second")
time.sleep(1)
# [Move 1: Move from top to bottom "|"]
waypoints.append(moving(wpose, 0, 0, -0.2))
# [Move 2: Move up to the middle "|"]
waypoints.append(moving(wpose, 0, 0, 0.1))
# [Move 3: Move right for the middle line "-"]
waypoints.append(moving(wpose, 0, 0.1, 0))
# [Move 4: Move up for the top of the right vertical line "|"]
waypoints.append(moving(wpose, 0, 0, 0.1))
# [Move 5: Move down from the top of right vertical line "|"]
waypoints.append(moving(wpose, 0, 0, -0.2))
draw_now(waypoints)