# Aaric Han ROS Midterm Project

This repo contains the python script to draw my intials on a UR5 robot

Terminal Commands (open in separate Terminal tabs)
```bash
roslaunch ur_gazebo ur5.launch limited:=true
```
```bash
roslaunch ur5_moveit_config ur5_moveit_planning_execution.launch sim:=true limited:=true
```
```bash
python3 initials.py
```

10/06/22 - Finished the `initials.py` which draws my first initial "A" with the end effector on the UR5 robot.

10/14/22 - Finished drawings for all 3 initials "ALH" with the end effector on the UR5 robot.